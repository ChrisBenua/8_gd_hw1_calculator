#include <memory>
#include <string>
#include <stdexcept>
#include <vector>
#include <unordered_map>
#include <regex>
#include <cmath>

class ICalculator
{
public:
    virtual double processOperation(const std::string& operationName, const std::vector<double>& args) const = 0;
    virtual bool exists(const std::string& operationName) const = 0;
    virtual int getArity(const std::string& operationName) const = 0;

    virtual ~ICalculator() = default;
};

class IConsoleManager
{
public:
    virtual void interact() = 0;

    virtual ~IConsoleManager() = default;
};

class ConsoleManager : public IConsoleManager
{
public:

    explicit ConsoleManager(std::unique_ptr<ICalculator> calculator)
    {
        _calculator = std::move(calculator);
    }

    void interact() override;

protected:
    std::unique_ptr<ICalculator> _calculator;

private:
    const std::regex operandRegex = std::regex("(\\+|-)?[0-9]+[\\.]??[0-9]*");

    bool correctOperand(const std::string& operand) const;

    std::string readOperator() const;

    double readOperand(int number) const;
};

class IOperation
{
public:
    static constexpr double epsilon = 1e-9;

    IOperation(){};

	virtual ~IOperation() = default;

	virtual double calculate(const std::vector<double>& args) const = 0;

	virtual int getArity() const = 0;
};

class Addition :
	public IOperation
{
public:
	double calculate(const std::vector<double>& args) const override
    {
		if (getArity() != args.size())
			throw std::invalid_argument("Invalid amount of arguments");
		return args[0] + args[1];
	}

	int getArity() const override
	{
		return 2;
	}
};

class Subtraction :
	public IOperation
{
public:
	double calculate(const std::vector<double>& args) const override
    {
		if (getArity() != args.size())
			throw std::invalid_argument("Invalid amount of arguments");
		return args[0] - args[1];
	}

	int getArity() const override
	{
		return 2;
	}
};

class Multiplication :
	public IOperation
{
public:
	double calculate(const std::vector<double>& args) const override
	{
		if (getArity() != args.size())
			throw std::invalid_argument("Invalid amount of arguments");
		return args[0] * args[1];
	}

	int getArity() const override
	{
		return 2;
	}
};

class Division :
	public IOperation
{
public:
	double calculate(const std::vector<double>& args) const override
	{
		if (getArity() != args.size())
			throw std::invalid_argument("Invalid amount of arguments");
		if (std::abs(args[1]) <= epsilon)
			throw std::overflow_error("Divide by zero exception");
		return args[0] / args[1];
	}

	int getArity() const override
	{
		return 2;
	}
};

class Pow :
        public IOperation
{
public:
    double calculate(const std::vector<double>& args) const override {
        if (getArity() != args.size())
            throw std::invalid_argument("Invalid amount of arguments");
        if (args[0] < 0 && args[1] != (int) args[1])
            throw std::invalid_argument("Attempt to raise a negative number to a fractional power");
        return pow(args[0], args[1]);
    }
    
    int getArity() const override
    {
        return 2;
    }
};

class DivMod :
        public IOperation
{
public:
    double calculate(const std::vector<double>& args) const override {
        if (getArity() != args.size())
            throw std::invalid_argument("Invalid amount of arguments");
        if (args[0] != (int) args[0] || args[1] != (int) args[1])
            throw std::invalid_argument("Not an integer exception");
		if (std::abs(args[1]) <= epsilon)
			throw std::overflow_error("Divide by zero exception");
        return (int) args[0] % (int) args[1];
    }
    
    int getArity() const override
    {
        return 2;
    }
};

class Factorial :
        public IOperation
{
public:

    double calculate(const std::vector<double>& args) const override
    {
        if (getArity() != args.size())
            throw std::invalid_argument("Invalid amount of arguments");
        if ((int) args[0] != args[0])
            throw std::invalid_argument("Not an integer exception");
        if ((int) args[0] < 0)
            throw std::invalid_argument("Negative integer exception");
        if ((int) args[0] >= MAX_SIZE)
            throw std::overflow_error("Double overflow exception");

        return factorial[(int) args[0]];
    }

    int getArity() const override
    {
        return 1;
    }

private:
    static constexpr int MAX_SIZE = 171;
    static const std::vector<double> factorial;
};

const std::vector<double> Factorial::factorial = [] {
        std::vector<double> factorial(MAX_SIZE);
        factorial[0] = 1;
        for (int i = 1; i < MAX_SIZE; ++i)
            factorial[i] = factorial[i - 1] * i;
        return factorial;
}();


class Sine :
        public IOperation
{
public:
    double calculate(const std::vector<double>& args) const override
    {
        if (getArity() != args.size())
            throw std::invalid_argument("Invalid amount of arguments");
        return sin(args[0]);
    }

    int getArity() const override
    {
        return 1;
    }
};

class Sqrt :
        public IOperation {
    double calculate(const std::vector<double> &args) const override {
        if (getArity() != args.size())
            throw std::invalid_argument("Invalid amount of arguments");
        if (args[0] < 0)
            throw std::invalid_argument("Attempt to get square root of negative value!");
        else
            return sqrt(args[0]);
    }


    int getArity() const override
    {
        return 1;
    }
};

class Cosine :
        public IOperation
{
public:

    double calculate(const std::vector<double>& args) const override
    {
        if (getArity() != args.size())
            throw std::invalid_argument("Invalid amount of arguments");
        return cos(args[0]);
    }

    int getArity() const override
    {
        return 1;
    }
};

class Logarithm :
        public IOperation
{
    double calculate(const std::vector<double>& args) const override {
        if (getArity() != args.size())
            throw std::invalid_argument("Invalid amount of arguments");
        if (args[0] < epsilon)
            throw std::invalid_argument("Attempt to get logarithm of non-positive value!");
        else
            return log(args[0]);

    }

    int getArity() const override
    {
        return 1;
    }
};

class Tangent :
        public IOperation
{
public:

    double calculate(const std::vector<double>& args) const override
    {
        if (getArity() != args.size())
            throw std::invalid_argument("Invalid amount of arguments");
        double remainder = fmod(std::abs(args[0]), M_PI / 2);
        if (remainder <= epsilon || M_PI / 2 - remainder <= epsilon)
        {
            throw std::invalid_argument("Attempt to evaluate Tangent extremely near pi/2+pi*k");
        }

        return tan(args[0]);
    }

    int getArity() const override
    {
        return 1;
    }
};

class ICalculatorConfig
{
public:
    virtual ICalculatorConfig&
    addOperation(const std::string& operationName, std::unique_ptr<IOperation> operation) = 0;

    virtual const IOperation* getOperation(const std::string& operationName) const = 0;

    virtual bool exists(const std::string& operationName) const = 0;

    virtual ~ICalculatorConfig() = default;
};

class CalculatorConfig : public ICalculatorConfig
{
public:
    CalculatorConfig& addOperation(const std::string& operationName, std::unique_ptr<IOperation> operation) override;

    const IOperation* getOperation(const std::string& operationName) const override;

    bool exists(const std::string& operationName) const override;

private:
    std::unordered_map<std::string, std::unique_ptr<IOperation>> _operationForNames;
};

CalculatorConfig&
CalculatorConfig::addOperation(const std::string& operationName, std::unique_ptr<IOperation> operation)
{
    _operationForNames.emplace(operationName, std::move(operation));
    return *this;
}

const IOperation* CalculatorConfig::getOperation(const std::string& operationName) const
{
    if (CalculatorConfig::exists(operationName))
        return _operationForNames.at(operationName).get();
    else
        return nullptr;
}

bool CalculatorConfig::exists(const std::string& operationName) const
{
    return _operationForNames.count(operationName) > 0;
}


class Calculator : public ICalculator
{
public:
    explicit Calculator(std::unique_ptr<ICalculatorConfig> _config)
    {
        this->_config = std::move(_config);
    }

    double processOperation(const std::string& operationName, const std::vector<double>& args) const override;

    bool exists(const std::string& operationName) const override;

    int getArity(const std::string& operationName) const override;

private:
    std::unique_ptr<ICalculatorConfig> _config;
};

double Calculator::processOperation(const std::string& operationName, const std::vector<double>& args) const
{
    return _config->getOperation(operationName)->calculate(args);
}

bool Calculator::exists(const std::string& operationName) const
{
    return _config->exists(operationName);
}

int Calculator::getArity(const std::string& operationName) const
{
    return _config->getOperation(operationName)->getArity();
}
