#include <iostream>
#include "calculator.h"
#include <string>
#include <regex>
#include <vector>
#include <iomanip>

ConsoleManager* createManager();
CalculatorConfig* createConfig();

int main()
{
    ConsoleManager* manager = createManager();
    manager->interact();
    return 0;
}

ConsoleManager* createManager() {
    auto* config = createConfig();
    auto* calculator = new Calculator(std::unique_ptr<CalculatorConfig>(config));
    auto* manager = new ConsoleManager(std::unique_ptr<Calculator>(calculator));
    return manager;
}

CalculatorConfig* createConfig() {
    auto* config = new CalculatorConfig();
    config->addOperation("+", std::unique_ptr<Addition>(new Addition()))
    .addOperation("-", std::unique_ptr<Subtraction>(new Subtraction()))
    .addOperation("*", std::unique_ptr<Multiplication>(new Multiplication()))
    .addOperation("/", std::unique_ptr<Division>(new Division()))
    .addOperation("%", std::unique_ptr<DivMod>(new DivMod()))
    .addOperation("pow", std::unique_ptr<Pow>(new Pow()))
    .addOperation("!", std::unique_ptr<Factorial>(new Factorial()))
    .addOperation("sin", std::unique_ptr<Sine>(new Sine()))
    .addOperation("log", std::unique_ptr<Logarithm>(new Logarithm()))
    .addOperation("sqrt", std::unique_ptr<Sqrt>(new Sqrt()))
    .addOperation("cos", std::unique_ptr<Cosine>(new Cosine()))
    .addOperation("tan", std::unique_ptr<Tangent>(new Tangent()));

    return config;
}

void ConsoleManager::interact()
{
    std::cout << "Enter 0 to exit\n";
    std::cout << std::fixed << std::setprecision(10);
    while (true)
    {
        std::string inputOperator = readOperator();
        if (inputOperator == "0")
            return;
        int arity = _calculator->getArity(inputOperator);

        std::vector<double> operands(arity);
        for (int i = 0; i < arity; ++i)
            operands[i] = readOperand(i);

        try
        {
            double result = _calculator->processOperation(inputOperator, operands);
            std::cout << "Result: " << result << '\n';
        }
        catch (std::invalid_argument&)
        {
            std::cout << "Invalid argument!" << std::endl;
        }
        catch (std::overflow_error&)
        {
            std::cout << "Invalid argument!" << std::endl;
        }
    }
}

bool ConsoleManager::correctOperand(const std::string& operand) const
{

    bool correct = std::regex_match(operand, operandRegex);
    if (correct)
    {
        try
        {
            std::stod(operand);
        }
        catch (std::out_of_range&)
        {
            correct = false;
        }
        catch (std::invalid_argument&)
        {
            correct = false;
        }
    }
    return correct;
}

std::string ConsoleManager::readOperator() const
{
    std::cout << "Enter operator: ";
    std::string inputOperator;
    while(std::cin >> inputOperator && !(_calculator->exists(inputOperator) || (inputOperator == "0")))
    {
        std::cout << "Operator doesn\'t exist. Try again\n";
        std::cout << "Enter operator: ";
    }
    return inputOperator;
}

double ConsoleManager::readOperand(int number) const
{
    std::cout << "Operand № " << number + 1 << ": ";
    std::string operand;
    while(std::cin >> operand && !correctOperand(operand))
    {
        std::cout << "Invalid operand. Try again\n";
        std::cout << "Operand № " << number + 1 << ": ";
    }
    return std::stod(operand);
}